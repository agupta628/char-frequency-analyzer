# Character Frequency Analyzer

I'm buying a new keyboard, the [Ergodox EZ](https://ergodox-ez.com/). One of
the reasons whhy I'm buying the keyboard, other than ergonomics, is due to 
its insane customiziblity. This script will analyze all of the code that I have
written to determine what characters I type the most often.

Once I have that data, I can create a keyboard that is designed for me, by me.